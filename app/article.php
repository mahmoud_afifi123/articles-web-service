<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class article extends Model
{
    protected $table = 'articles';

    protected $fillable = ['title', 'images', 'details', 'writer', 'article_location', 'my_location', 'article_video', 'user_id'];

    public function users()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
