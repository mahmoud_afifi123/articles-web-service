<?php

namespace App\Http\Controllers;

use App\article;
use Illuminate\Http\Request;

class clientController extends Controller
{
    public function index()
    {
        $all_articles=article::with('users')->get();
        return view('client.home',compact('all_articles'));
    }
    public function newsDetails($id)
    {
        $specific_article=article::with('users')->find($id);
        $images_of_specific_article=explode(',',$specific_article->images);
        return view('client.details',compact('specific_article','images_of_specific_article'));
    }
}
