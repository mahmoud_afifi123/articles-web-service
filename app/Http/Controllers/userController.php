<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use OneSignal;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class userController extends Controller
{
    public function __construct()
    {
        $this->content = array();
    }

    public function login(Request $request)
    {
        $cardiationals = $request->only('email', 'password');
        try {
            if (!$token = JWTAuth::attempt($cardiationals)) {
                return response()->json(['msg' => 'invalid'], 401);
            }
        } catch (JWTException $e) {
            return respone()->json(['msg' => 'can not create token']);
        }
        return response()->json(['token'=>$token]);
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
            'phone_number' => 'required|min:12',
        ]);
        $new_user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'phone_number' => $request->input('phone_number'),
        ]);
        if ($new_user) {
            return response()->json(['msg' => 'user saved succeffully']);
        } else {
            return response()->json(['msg' => 'error, user can not be saved']);
        }

    }
}
