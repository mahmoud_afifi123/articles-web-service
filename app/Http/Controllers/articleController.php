<?php

namespace App\Http\Controllers;

use App\article;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use OneSignal;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class articleController extends Controller
{
    public function __construct()
    {
        $this->middleware('JWT.auth',[
	   'only'=>['store','destroy']
	   ]);
    }

    public static $article = null;

    //function of enforce that one article object is used in all controller
    public function checkSingltonObject()
    {
        if (static::$article == null) {
            static::$article = new article();
        }
        return static::$article;
    }

    public function store(Request $request)
    {
        $images = [];
        $this->checkSingltonObject();

        //recieving inputs from web and devices
        $title = $request->input('title');
        $uploaded_images = $request->file('images');
        $details = $request->input('details');
        $writer = $request->input('writer');
        $article_location = $request->input('article_location');
        $my_location = $request->input('my_location');
        $article_video = $request->input('article_video');

        //validation of inputs

        $this->validate($request,[
            'title' => 'required|max:250',
            'images' => 'required',
            'details' => 'required',
            'writer' => 'required',
            'article_location' => 'required',
            'my_location' => 'required',
            'article_video' => 'required',
        ]);
        //save uploded images of articles in the uploads folder
        foreach ($uploaded_images as $image) {
            $filename = rand(0, 3000) . time() . $image->getClientOriginalName();
            $image->move('uploads', $filename);
            $images[] = $filename;
        }

        //saving the inputs from web and devices about article in article table

        /*OneSignal::sendNotificationToAll("Some Message", $url = null, $data = null, $buttons = null, $schedule = null);*/
        if(!$user=JWTAuth::parseToken()->authenticate())
        {
            return response()->json(['msg'=>"user not found"]);

        }
        else {
            static::$article->create(['title' => $title, 'images' => implode(',', $images), 'details' => $details, 'writer' => $writer, 'article_location' => $article_location,
                'my_location' => $my_location, 'article_video' => $article_video ,'user_id'=>$user->id]);

            //return json response to web and devices
            $data = ["title" => $title, 'images' => $images, 'details' => $details, 'writer' => $writer, 'article_location' => $article_location
                , 'my_location' => $my_location, 'article_video' => $article_video,'user_name'=>$user->name];
        }
        $users=User::get();
        foreach ($users as $u)
        {
            Notification::send($u, new \App\Notifications\articleCreated());
        }

        return response()->json(
            [
                'message' => 'article created successfully',
                'data' => $data
            ]
        );
    }

    //function to get specific article
    public function getSpecificArticle($id)
    {
        $this->checkSingltonObject();
        //retrieve data about specific article id
        $article_data = static::$article->find($id);
        //return json response contain data about specific article
        if (count($article_data) > 0) {
            return response()->json(
                [
                    'message' => 'specific article retrieved successfully',
                    'data' => $article_data
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => 'error, can not get response with specific article',

                ]
            );
        }
    }

    //function to get all article
    public function getAllArticles()
    {
        $this->checkSingltonObject();
        //retrieve data about all articles
        $all_articles_data = static::$article->get();
        //return json response contain data about all articles
        if (count($all_articles_data) > 0) {
            return response()->json(
                [
                    'message' => 'all articles retrieved successfully',
                    'data' => $all_articles_data
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => 'error, can not get response with articles',
                ]
            );
        }
    }

    public function destroy($id)
    {
        $this->checkSingltonObject();
        if (static::$article->find($id)->delete()) {
            return response()->json(
                [
                    'message' => 'this article deleted successfully'
                ]
            );
        } else {
            return response()->json(
                [
                    'message' => 'error, can not delete the article'
                ]
            );
        }

    }

}
