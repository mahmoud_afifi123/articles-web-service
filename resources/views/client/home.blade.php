@include('layouts.client_header')

<div style="margin-top: 50px; margin-left: 350px;">
    <div class="col-md-8">
        @foreach($all_articles as $article)
            <div class="row">
                <a href="{{ url('/details/'.$article->id) }}">
                    <div class="thumbnail">
                        <img src="{{ url('uploads/'.explode(',',$article->images)[0]) }}"
                             style="height: 150px; width: 300px;">
                        <div class="caption">
                            <h3>
                                {{ $article->title }}
                            </h3>
                            <h4>
                                added by:  {{ $article->users->name }} with number {{ $article->users->phone_number }}
                            </h4>
                            <p>
                                article location:<a
                                        href="{{$article->article_location}}}">{{ substr($article->article_location,0,30) }}
                                    ..........</a>
                            </p>
                            <p>
                                article video:<a
                                        href="{{$article->article_video}}}">{{ substr($article->article_video,0,30) }}
                                    ..........</a>
                            </p>
                            <p>
                            </p>
                        </div>
                    </div>
                </a>
            </div>
        @endforeach
    </div>
</div>


