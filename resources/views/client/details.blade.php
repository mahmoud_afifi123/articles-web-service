<div class="container">

    <div class="row">


        <div class="col-md-14">

            <div class="thumbnail" style="margin-left: 50px;">
                {{--this is the main image--}}

                <img style="width: 608px; height: 100px; margin-left: 310px;" src="{{ url('uploads/'.$images_of_specific_article[0]) }}" alt=""><br><br>
                <div style="margin-left: 310px;">
                    {{--this is the secondary images--}}
                    @foreach($images_of_specific_article as $sec_img)
                        <img style="width: 200px; height: 100px;" src="{{ url('uploads/'.$sec_img) }}">
                    @endforeach
                </div>
                <div class="caption">

                    <h1 style="margin-left: 420px;">
                        {{ $specific_article->title }}
                    </h1>
                    <h4>
                        added by:  {{ $specific_article->users->name }} with number {{ $specific_article->users->phone_number }}
                    </h4>
                    <p>
                        article location:<a href="{{$specific_article->article_location}}}">{{ substr($specific_article->article_location,0,30) }}..........</a>
                    </p>
                    <p>
                        My location:<a href="{{$specific_article->my_location}}}">{{ substr($specific_article->my_location,0,30) }}..........</a>
                    </p>
                    <p>
                        article video:<a href="{{$specific_article->article_video}}}">{{ substr($specific_article->article_video,0,30) }}..........</a>
                    </p>
                    <p>
                        {{ $specific_article->details }}
                    </p>
                    <p>
                    </p>
                </div>
            </div>


        </div>

    </div>

</div>
    