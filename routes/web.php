<?php

Route::group(['prefix'=>'api/admin'],function (){

  Route::post('article/store','articleController@store');
  Route::get('article/{id}','articleController@getSpecificArticle');
  Route::get('article','articleController@getAllArticles');
  Route::get('article/delete/{id}','articleController@destroy');

});

Route::group(['prefix'=>'api'],function ()
{
   Route::post('user/register','userController@register');
   Route::post('user/login','userController@login');
});

//about the client
Route::get('/','clientController@index');
Route::get('/details/{id}','clientController@newsDetails');

